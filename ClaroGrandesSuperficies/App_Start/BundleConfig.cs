﻿using System.Web;
using System.Web.Optimization;

namespace ClaroGrandesSuperficies
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/Scripts/jquery-1.12.4.min.js",
                "~/Content/select2/select2.min.js",
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/jquery-ui-1.12.1.min.js",
                "~/Scripts/jquery-slimscroll/jquery.slimscroll.min.js",
                "~/Scripts/fastclick/lib/fastclick.js",
                "~/Scripts/adminlte.min.js",
                "~/Scripts/app/globalesBootsrap.js",
                "~/Scripts/app/app.js",
                "~/Scripts/sweetalert2.js"

            ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            "~/Scripts/jquery.validate*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      //"~/Content/themes/base/jquery-ui.min.css",
                      "~/Content/select2/select2.min.css",
                      "~/Content/bootstrap.min.css",
                      "~/Content/font-awesome/css/font-awesome.min.css",
                      "~/Content/fonts/css/ionicons.min.css",
                      "~/Content/adminlte/css/AdminLTE.min.css",
                      "~/Content/adminlte/css/skins/_all-skins.min.css",
                      "~/Content/Site.css"
            ));
        }
    }
}
