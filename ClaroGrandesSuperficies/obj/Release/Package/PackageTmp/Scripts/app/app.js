﻿DEV_MODE = true;
SHOW_ALERT = false;
function _console(msg) {
    if (DEV_MODE == false) { return };
    SHOW_ALERT ? alert(msg) : console.log(msg);
}
$(document).ready(function () {
    $('.sidebar-menu').tree()
    $(".datepicker").datepicker();
    $(".select2").select2();
})


class General {
    constructor() {
        this.url = {};
        this.url.Customer = BaseUrl + "Customers/";
        this.url.PointOfCare = BaseUrl + "PointOfCares/";
        this.url.Sales = BaseUrl + "sales/";
        this.url.Inventory = BaseUrl + "Inventories/";
        
        this.data = {};

    }

    PopulateDropdown(Result) {
        $("#CityId").append('<option value="0">Sin Seleccionar</option>');
        $.each(Result, function (index, Value) {
            $("#CityId").append('<option value="'
                + Value.Value + '">' +
                Value.Text + '</option>');
        });
    }

    JsonDecodeErrors(ErrorObject) {
        if (ErrorObject.Type == 0) {
            return string_mensaje(ErrorObject.Message,"alert-danger");
        }
        else
            return string_mensaje(ErrorObject.Message + "[" + ErrorObject.Name + "]" , "alert-danger");
    }

}

$("body").on("input", ".chkPointGeneral", function() {
    _console("entranto a check point")
    val = $(this).val();
    $.post(BaseUrl + "Sales/" + 'UpdateCurrentPoint', {Id : val})
        .done(Result => {
            _console("Ok");
        })
        .fail((data, status) => {
            _console("Error interno favor contactar al administrador");
        })
})