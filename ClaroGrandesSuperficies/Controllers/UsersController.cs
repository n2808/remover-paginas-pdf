﻿using ClaroGrandesSuperficies.Models;
using Core.Models.Security;
using Core.Models.User;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ClaroGrandesSuperficies.Controllers
{

    public class UsersController : Controller
    {
        private ContextModel db = new ContextModel();



        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Document,Names")] User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    user.Id = Guid.NewGuid();
                    user.Status = true;
                    db.Users.Add(user);
                    db.SaveChanges();
                    return RedirectToAction("Create", "DeleteFiles", new { Message = 1 });
                }
                catch (Exception)
                {

                    throw;
                }

            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Document,Names,Status")] User user, string Status)
        {

            try
            {
                var userEdit = db.Users.Where(x => x.Id == user.Id).FirstOrDefault();
                userEdit.Status = Status == "1" ? true : false;  
                userEdit.Document = user.Document;
                userEdit.Names = user.Names;

                db.Entry(userEdit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Create", "DeleteFiles", new { Message = 2 });
               
            }
            catch (Exception)
            {

                throw;
            }


        }



        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }
                var user = db.Users.Where(x => x.Id == id).FirstOrDefault();
                user.Status = false;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Create", "DeleteFiles", new { Message = 3 });

            }
            catch (Exception)
            {

                throw;
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }




    }
}
