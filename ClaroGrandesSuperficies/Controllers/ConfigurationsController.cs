﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClaroGrandesSuperficies.Models;
using Core.Models.configuration;
using Core.Models.Security;

namespace ClaroGrandesSuperficies.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class ConfigurationsController : Controller
    {

        private ContextModel db = new ContextModel();



        // GET: Configurations

        public ActionResult Index(Guid? Category, string Name)
        {
            Category ActualCategory = db.Categories.Find(Category);

            if (ActualCategory == null || ActualCategory.CanChanged == false)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "la category no existe.");
            }

            ViewBag.Name = ActualCategory.Name;
            ViewBag.FName = Name;
            ViewBag.ColumName = ActualCategory.Name;
            ViewBag.Category = Category;
            var configurations = db.Configurations.Include(c => c.Category).Where(C => C.CategoriesId == Category);
            if (Name != null && Name.Trim() != "")
            {
                configurations = configurations.Where(c => c.Name.Contains(Name));
            }

            return View(configurations.ToList());
        }

        // GET: Configurations/Details/5
        //[CustomAuthorize(Roles = "Admin")]
        [CustomAuthorize(Roles = "7E6B69E8-87D0-419F-9054-02FCA94D5A73")]
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Configuration configurations = db.Configurations.Include(c => c.Category).Where(c => c.Id == id).FirstOrDefault();
            if (configurations == null)
            {
                return HttpNotFound();
            }
            return View(configurations);
        }

        // GET: Configurations/Create
        //[CustomAuthorize(Roles = "Admin")]
        [CustomAuthorize]
        public ActionResult Create(Guid? Category)
        {
            Category ActualCategory = db.Categories.Find(Category);
            if (ActualCategory == null || ActualCategory.CanChanged == false)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "La Categoria no existe.");
            }

            ViewBag.Category = Category;
            ViewBag.Name = ActualCategory.Name;
            ViewBag.Guid = ActualCategory.Name;
            ViewBag.Configtype = ActualCategory.Name;
            ViewBag.ColumName = ActualCategory.Name;
            return View();
        }

        // POST: Configurations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Status,CategoriesId,Description,GuidId")] Configuration configurations)
        {
            if (ModelState.IsValid)
            {

                db.Configurations.Add(configurations);
                db.SaveChanges();
                return RedirectToAction("Index", new { Category = configurations.CategoriesId });
            }
            ViewBag.GuidId = new SelectList(db.Configurations, "Id", "Name", configurations.GuidId);
            ViewBag.CategoriesId = new SelectList(db.Categories, "Id", "Name", configurations.CategoriesId);
            return View(configurations);
        }

        // GET: Configurations/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Configuration configurations = db.Configurations.Find(id);
            if (configurations == null)
            {
                return HttpNotFound();
            }

            return View(configurations);
        }

        // POST: Configurations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Status,Description,GuidId")] Configuration configurations)
        {
            if (ModelState.IsValid)
            {

                db.Entry(configurations).State = EntityState.Modified;
                db.Entry(configurations).Property(c => c.CategoriesId).IsModified = false;
                db.SaveChanges();

                ContextModel db2 = new ContextModel();
                var CategoryId = db2.Configurations.Find(configurations.Id).CategoriesId;


                return RedirectToAction("Index", new { Category = CategoryId });
            }

            return View(configurations);
        }

        [CustomAuthorize]       
        public ActionResult ListEmail()
        {
            List<Configuration> configuration = db.Configurations.Include(c => c.Category).Where(x => x.CategoriesId == new Guid("2F75E8B6-C8B5-44D7-9BDC-1C90B6E55EC8")
                                                || x.CategoriesId == new Guid("AACD2C34-B89B-412A-8037-F0596F64420B")).ToList();
            return View(configuration);
        }


        public ActionResult CreateEmail()
        {
            ViewBag.CategoriesId = new SelectList(db.Categories.OrderBy(x => x.Name).Where(x => x.Id == new Guid("2F75E8B6-C8B5-44D7-9BDC-1C90B6E55EC8")
                                                || x.Id == new Guid("AACD2C34-B89B-412A-8037-F0596F64420B")), "Id", "Name");

            return View();
        }

        [HttpPost]
        public ActionResult CreateEmail(Configuration configurations)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    configurations.Name = configurations.Name.Trim();
                    db.Configurations.Add(configurations);
                    db.SaveChanges();
                    return RedirectToAction("ListEmail");
                }
                catch (Exception)
                {

                    throw;
                }
            }

            ViewBag.CategoriesId = new SelectList(db.Categories.OrderBy(x => x.Name).Where(x => x.Id == new Guid("2F75E8B6-C8B5-44D7-9BDC-1C90B6E55EC8")
                                              || x.Id == new Guid("AACD2C34-B89B-412A-8037-F0596F64420B")), "Id", "Name", configurations.CategoriesId);
            return View(configurations);
        }

        public ActionResult EditEmail(Guid? Id)
        {
            Configuration configuration = db.Configurations.Find(Id);

            if (configuration == null)
            {
                return HttpNotFound();
            }

            ViewBag.CategoriesId = new SelectList(db.Categories.OrderBy(x => x.Name).Where(x => x.Id == new Guid("2F75E8B6-C8B5-44D7-9BDC-1C90B6E55EC8")
                                                || x.Id == new Guid("AACD2C34-B89B-412A-8037-F0596F64420B")), "Id", "Name", configuration.CategoriesId);

            return View(configuration);
        }

        [HttpPost]
        public ActionResult EditEmail(Configuration configuration)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Configuration model = db.Configurations.Where(x => x.Id == configuration.Id).FirstOrDefault();
                    model.Name = configuration.Name.Trim();
                    model.Description = configuration.Description;
                    model.Status = configuration.Status;
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("ListEmail");

                }
                catch (Exception)
                {

                    throw;
                }
            }

            ViewBag.CategoriesId = new SelectList(db.Categories.OrderBy(x => x.Name).Where(x => x.Id == new Guid("2F75E8B6-C8B5-44D7-9BDC-1C90B6E55EC8")
                                                || x.Id == new Guid("AACD2C34-B89B-412A-8037-F0596F64420B")), "Id", "Name", configuration.CategoriesId);

            return View(configuration);
        }



        // GET: Configurations/Delete/5
        public ActionResult Delete(Guid? id, int Configtype = 0)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Configuration configurations = db.Configurations.Include(c => c.Category).Where(c => c.Id == id).FirstOrDefault();
            if (configurations == null)
            {
                return HttpNotFound();
            }

            return View(configurations);
        }

        // POST: Configurations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id, int Configtype = 0)
        {
            Configuration configurations = db.Configurations.Find(id);
            db.Configurations.Remove(configurations);
            db.SaveChanges();
            return RedirectToAction("Index", new { Category = configurations.CategoriesId });
        }

        public JsonResult MailCheck(string email, Guid? category, string action, Guid? CofigurationId)
        {
            if (action == "1")
            {
                // Validación al crear.
                var resp = db.Configurations.Where(x => x.Name.Trim() == email.Trim() && x.CategoriesId == category).FirstOrDefault();
                if (resp != null)
                {
                    return Json(new { status = false }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { status = true }, JsonRequestBehavior.AllowGet);

                }
            }
            else
            {
                // Validación al editar.
                var emailConf = db.Configurations.Where(x => x.Id == CofigurationId && x.CategoriesId == category).Select(x => x.Name).FirstOrDefault();
                if (email.Trim() == emailConf)
                {
                  
                    return Json(new { status = true }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    var emails = db.Configurations.Where(x => x.CategoriesId == category
                                                           && x.Name != emailConf).Select(x => x.Name).ToList();
                    foreach (var item in emails)
                    {
                        if (item == email.Trim())
                        {
                            return Json(new { status = false }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    return Json(new { status = true }, JsonRequestBehavior.AllowGet);

                }
            
            }
          
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }
}
