﻿using ClaroGrandesSuperficies.Models;
using ClaroGrandesSuperficies.Models.SendEmail;
using Core.Models.Security;
using iTextSharp.text.pdf;
using Syncfusion.Pdf.Parsing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;




namespace ClaroGrandesSuperficies.Controllers
{
    public class AccountsController : Controller
    {

        private ContextModel db = new ContextModel();
        // GET: Accounts
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(AccountsViewModel avm)
        {

            AccountsModel am = new AccountsModel();
            Accounts ResultUser = am.Login(avm.Accounts.UserName, avm.Accounts.Password);
            if (string.IsNullOrEmpty(avm.Accounts.UserName) || string.IsNullOrEmpty(avm.Accounts.Password) || ResultUser == null)
            {
                ViewBag.Error = "Usuario o contraseña incorrectos";
                return View("index");
            }
            // get user data for the session.
            SessionPersister.UserName = avm.Accounts.UserName;
            SessionPersister.Id = ResultUser.Id;
            SessionPersister.PointOfCare = ResultUser.PointOfAtention;
            if (SessionPersister.PointOfCare.ToList().Count == 1)
            {
                SessionPersister.CurrentPointOfCare = SessionPersister.PointOfCare.FirstOrDefault();
            }

            return RedirectToAction("Home", "Sales", null);

        }

        public ActionResult Logout()
        {
            SessionPersister.UserName = string.Empty;
            SessionPersister.Id = null;
            SessionPersister.PointOfCare = null;
            SessionPersister.CurrentPointOfCare = null;
            return View("index");
        }



        //public ActionResult Create()
        //{
        //    var users = db.Users.Where(x => x.Status).ToList();

        //    return View(users);
        //}

        //[HttpPost]
        //public JsonResult Create(HttpPostedFileBase File, string Email)
        //{

        //    try
        //    {

        //        if (File != null)
        //        {
        //            List<int> Pages = new List<int>();
        //            List<string> documents = db.Users.Where(x => x.Status).Select(x => x.Document).ToList();

        //            var newFilename = File.FileName;
        //            string guid = Guid.NewGuid().ToString();
        //            newFilename = $"Nómina_{guid}.pdf";


        //            //Eliminamos los archivos del directorio.
        //            //string directory = Path.Combine(Server.MapPath("~/File"));
        //            //DeleteFiles(directory);

        //            var extension = Path.GetExtension(File.FileName);
        //            if (extension != ".pdf")
        //            {
        //                return Json(new { status = 404, message = "Archivo con extención no válida" }, JsonRequestBehavior.AllowGet);

        //            }

        //            // Guardamos el nuevo archivo.   
        //            string path = Path.Combine(Server.MapPath("~/File"), Path.GetFileName(newFilename));
        //            File.SaveAs(path);

        //            // Proceso para buscar por número de documento del usuario.

        //            for (int j = 0; j < documents.Count; j++)
        //            {                     

        //                string textToSearch = documents[j].Trim().ToString();
                        
        //                // Leyendo el documento.
        //                StringBuilder text = new StringBuilder();
        //                PdfReader pdfReader = new PdfReader(path);
        //                for (int i = 1; i <= pdfReader.NumberOfPages; i++)
        //                {
        //                    iTextSharp.text.pdf.parser.ITextExtractionStrategy strategy = new iTextSharp.text.pdf.parser.SimpleTextExtractionStrategy();
        //                    string currentText = iTextSharp.text.pdf.parser.PdfTextExtractor.GetTextFromPage(pdfReader, i, strategy);
        //                    currentText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText)));

        //                    var pageText = currentText.ToString();

        //                    var exists = pageText.Contains(textToSearch);                            

        //                    if (exists)
        //                    {
        //                        var pagina = (i);
        //                        Pages.Add(pagina);
        //                    }
        //                }

        //                pdfReader.Close();

        //            }

        //            PdfLoadedDocument loadedDocument = new PdfLoadedDocument(path);

        //            #region CODIGO_COMENTADO_VERSION_ANTERIOR.
        //            //Remove the first page in the PDF document
        //            //var indexx = Pages[0].Split(',');
        //            //var index = Pages[0].Split(',');
        //            //var total_pages = loadedDocument.Pages.Count;

        //            //var duplicateExists = index.GroupBy(n => n).Any(g => g.Count() > 1);

        //            //if (duplicateExists)
        //            //{
        //            //    return Json(new { status = 404, message = "Está ingresando páginas duplicadas" }, JsonRequestBehavior.AllowGet);

        //            //}

        //            //if (!SearchPage(index, total_pages))
        //            //{
        //            //    return Json(new { status = 404, message = "La página no existe" }, JsonRequestBehavior.AllowGet);

        //            //}

        //            // Ordenamos la lista de páginas.

        //            #endregion

        //            Pages.Sort();

        //            // Eliminamos las páginas encontradas.
        //            for (int i = 0; i < Pages.Count; i++)
        //            {
        //                var page = Pages[i];
        //                loadedDocument.Pages.RemoveAt(page - (i + 1));
        //            }


        //            loadedDocument.Save(path);
        //            loadedDocument.Dispose();
        //            loadedDocument.Close();

        //            //Envío de correo
        //            SendEmail(path, Email);
        //            return Json(new { status = 200, ruta = "File/" + newFilename }, JsonRequestBehavior.AllowGet);
        //        }

        //        return Json(new { status = 500 }, JsonRequestBehavior.AllowGet);

        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { status = 500, message = "No se ha podido crear el archivo" });
        //    }

        //}

        public bool SearchPage(string[] index, int total_pages)
        {
            // Obeniendo la página mayor del array.
            int max = int.Parse(index[0]);

            for (int i = 0; i < index.Length; i++)
            {
                if (int.Parse(index[i]) > max)
                {
                    max = int.Parse(index[i]);
                }
            }

            if (max > total_pages)
            {
                return false;

            }
            return true;
        }


        public void SendEmail(string path, string email)
        {

            var correo = new _Correo()
            {
                ServerName = "mail.uno27.com",
                port = "25",
                senderEmailId = "GS@uno27.com",
                senderPassword = "GS@uno27.com"
            };
            var bodyMessage = string.Empty;
            var mensaje = "Archivo nómina.";
            var correoEnvio = email;

            bodyMessage = "<!DOCTYPE html> <html lang='en'> <head> <meta charset='UTF-8'> <title>Nómina</title> </head> <body><p>" + mensaje + "</p><body></html>";
            Email.EnviarCorreoElectronico(correoEnvio, bodyMessage, "ARCHIVO DE NÓMINA", correo, path);

        }

        private void DeleteFiles(string pathFiles)
        {


            DirectoryInfo di = new DirectoryInfo(pathFiles);


            foreach (FileInfo fi in di.GetFiles())
            {
                var fullPath = $"{pathFiles}\\{ fi.Name}";



                System.IO.File.SetAttributes(fullPath, FileAttributes.Normal);
                System.IO.File.Delete(fullPath);


            }


        }


        public ActionResult AccessDenied()
        {
            return View();
        }

     
    }
}