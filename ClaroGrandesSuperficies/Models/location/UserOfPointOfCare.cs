﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Inventory
{
	public class UserOfPointOfCare : Entity
	{
        #region PROPERTIES
        [Required]
        [DisplayName("Vendedor")]
        public Guid SalesManId { get; set; }
        [Required]
        [DisplayName("punto de atencion")]
        public Guid PointOfCareId { get; set; }
        public bool Status { get; set; } = true;

        #endregion

        #region RELATIONS

        [ForeignKey("PointOfCareId")]
        public PointOfCare PointOfCare { get; set; }

        [ForeignKey("SalesManId")]
        public Core.Models.User.User SalesMan { get; set; }
        #endregion
    }
}