﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.location
{
	public class Country : Entity
	{
		public string Name { get; set; }
		public bool Status { get; set; }
		
	}
}