﻿using System;
using System.Net;
using System.Net.Mail;

namespace ClaroGrandesSuperficies.Models.SendEmail
{
    public static class Email
    {
        // Enviar Correo Electronico
        public static void EnviarCorreoElectronico(string Destinatario, string mensaje, string Asunto, _Correo CredencialesCorreo, string pathFile)
        {
            try
            {
                //var db = new ContextModel();
                var smtpServerName = CredencialesCorreo.ServerName;
                var port = CredencialesCorreo.port;
                var senderEmailId = CredencialesCorreo.senderEmailId;
                var senderPassword = CredencialesCorreo.senderPassword;

                MailMessage mail = new MailMessage();
                mail.From = new System.Net.Mail.MailAddress(senderEmailId);
                mail.Subject = Asunto;
                // The important part -- configuring the SMTP client
                SmtpClient smtp = new SmtpClient();
                smtp.Port = Int32.Parse(port);   // [1] You can try with 465 also, I always used 587 and got success
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network; // [2] Added this
                smtp.UseDefaultCredentials = false; // [3] Changed this
                smtp.Credentials = new NetworkCredential(senderEmailId, senderPassword);  // [4] Added this. Note, first parameter is NOT string.
                smtp.Host = smtpServerName;

                //recipient address . el problema es el correo electronico.
                mail.To.Add(new MailAddress(Destinatario));

                //Formatted mail body
                mail.IsBodyHtml = true;

                mail.Body = mensaje;
                //smtp.Send(mail);
                mail.Attachments.Add(new Attachment(pathFile));

                smtp.Send(mail);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Exception caught in CreateTimeoutTestMessage(): {0}",
                ex.ToString());
            }
        }

    }


    public class _Correo
    {
        public string ServerName { get; set; }
        public string port { get; set; }
        public string senderEmailId { get; set; }
        public string senderPassword { get; set; }
        public string alias { get; set; }

    }
}
