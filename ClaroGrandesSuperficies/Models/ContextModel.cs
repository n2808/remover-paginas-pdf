﻿namespace ClaroGrandesSuperficies.Models
{
  
    using Core.Models.configuration;
    using Core.Models.Inventory;
    using Core.Models.location;
    using Core.Models.User;
    using System.Data.Entity;

    public class ContextModel : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'ContextModel' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'ClaroGrandesSuperficies.Models.ContextModel' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'ContextModel'  en el archivo de configuración de la aplicación.
        public ContextModel()
            : base("name=DefaultConnection")
        {
        }

        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }                            


        #region LOCATIONS
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<PointOfCare> PointsOfCare { get; set; }
        public DbSet<UserOfPointOfCare> UsersOfPointOfCare { get; set; }

        #endregion


        #region CONFIGURATIONS
        public DbSet<Category> Categories { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        #endregion

        #region USERS
        public DbSet<User> Users { get; set; }
        public DbSet<Rol> Rols { get; set; }
        public DbSet<UserRol> UserRols { get; set; }
        #endregion



      


    }

}