﻿
$('body').on('click', '#btnCrear', function () {
    var category = $('#CategoriesId').val();
    var email = $('#Name').val();
    var description = $('#Description').val();
    var configurationId = $('#configurationId').val();
    var action = $('#btnCrear').data('action');   

    // #region Validación de Campos.
    if (category === "" || category === undefined) {
        $('#errorCategory').text('El campo categoría correo es requerido').css('color', 'red');
        return false;
    }
    if (email === "" || email === undefined) {
        $('#errorEmail').text('El campo correo es requerido').css('color', 'red');
        return false;
    }
    if (description === "" || description === undefined) {
        $('#errorDescription').text('El campo descripción es requerido').css('color', 'red');
        return false;
    }

    //#endregion
    mailCheck(email, category, action, configurationId);

});

// #region  Procedimiento para quitar las alertas.
$('body').on('change', '#CategoriesId', function () {
    $('#errorCategory').text('');
});
$('body').on('blur', '#Name', function () {
    $('#errorEmail').text('');
});
$('body').on('blur', '#Description', function () {
    $('#errorDescription').text('');
});
//#endregion

function mailCheck(data, Category, Action, configurationId) {
    $.post('/Configurations/' + 'MailCheck', { email: data, category: Category, action: Action, CofigurationId: configurationId })
        .done(function (Result) {
            if (Result.status == true) {
                Swal.fire({
                    icon: 'success',
                    title: 'Registro creado correctamente',
                    showConfirmButton: false,
                    timer: 1500
                });
                $("#btn_createEmail").click();
              
            } else {
                Swal.fire({
                    icon: 'info',
                    title: 'El correo ya existe',
                    showConfirmButton: false,
                    timer: 1500
                })

            }
        });
}
