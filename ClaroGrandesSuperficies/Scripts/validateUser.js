﻿$('body').on('click', '#btn_saveUser', function () {

    var document = $('#Document').val();
    var name = $('#Names').val();

    if (document == "") {
        $('.errorDocument').text("El número de documento está vacío").css('color', 'red');       
        return false;
    }

    if (name == "") {
        $('.errorName').text("El nombre de usuario está vacío").css('color', 'red');
        return false;
    }
});


$('body').on('blur', '#Document', function () {
    $('.errorDocument').text("");
});

$('body').on('blur', '#Names', function () {
    $('.errorName').text("");
});