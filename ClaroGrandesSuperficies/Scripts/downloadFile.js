﻿
// INSERT SALE INTO THE DATA
$("body").on("click", "#btn_createLog", function () {

    $(".alert").remove();
    var s_elm = $(this);

    var email = $("#email").val();

   
    if (email == "") {
        s_elm.after(string_mensaje("Debe ingresar el correo de destino"));
        return;
    }

    primerElementoG = 0;
    b_validacion = true;
    //este codigo hace la validacion
    $(".check_verificar").each(void_validaCampo);
    if (!b_validacion) { return false }

    $('#loading').css('display', 'block');

    var fd = new FormData();    
    fd.append('Email', email);
    fd.append('File', $('#file_pdf')[0].files[0]);

    $.ajax({
        url: BaseUrl + "DeleteFiles/" + 'Create',
        type: 'POST',
        data: fd,
        contentType: false,
        processData: false,
    })
        .done((Result) => {

            void_RedirectLogin(Result.status, Result.message)
            if (Result.status == 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Registro creado correctamente',
                    showConfirmButton: false,
                    timer: 1500
                }).then(function () {
                    location.reload();
                });

                $('#loading').css('display', 'none');
                $("a#" + "download").attr("href", Result.ruta);
                $("a#" + "download")[0].click();
            }
            else if (Result.status == 404) {
                Swal.fire({
                    icon: 'error',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1800
                });

                $('#loading').css('display', 'none');

            }
            else if (Result.status == 500) {
                $('#loading').css('display', 'none');
                s_elm.after(string_mensaje("El archivo no ha sido cargado"))
            }

            s_elm.attr('disabled', false);
        })
        .fail((Error) => {
            s_elm.after(string_mensaje("No se ha podido cargar el archivo"))
        })
        .always(function () {
            s_elm.attr('disabled', false);
        })

})